<?php

namespace CocoaStudios\AddressBook\Entry\Context\Concrete\Dialable
{
    use CocoaStudios\AddressBook\Entry\Contact\Type\Dialable;
    use CocoaStudios\AddressBook\Entry\Contact\Contract\Detail;
    use CocoaStudios\AddressBook\Entry\Contact\Type\Detail\Primary;

    /**
     * Class DialablePrimary
     *
     * @package CocoaStudios\AddressBook\Entry\Context\Concrete\Dialable
     */
    class DialablePrimary implements Detail
    {
        protected $value;

        public function __construct($value)
        {
            $this->value = $value;
        }

        public function getValue(): string
        {
            return $this->value;
        }

        public function getName(): string
        {
            return Dialable::NUMBER;
        }

        public function getType(): string
        {
            return Primary::PRIMARY;
        }
    }
}
