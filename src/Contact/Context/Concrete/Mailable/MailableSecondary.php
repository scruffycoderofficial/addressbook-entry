<?php

namespace CocoaStudios\AddressBook\Entry\Context\Concrete\Mailable
{
    use CocoaStudios\AddressBook\Entry\Contact\Type\Mailable;
    use CocoaStudios\AddressBook\Entry\Contact\Contract\Detail;
    use CocoaStudios\AddressBook\Entry\Contact\Type\Detail\Secondary;

    /**
     * Class MailableSecondary
     *
     * @package CocoaStudios\AddressBook\Entry\Context\Concrete\Mailable
     */
    class MailableSecondary implements Detail
    {
        protected $value;

        public function __construct($value)
        {
            $this->value = $value;
        }

        public function getValue(): string
        {
            return $this->value;
        }

        public function getName(): string
        {
            return Mailable::ADDRESS;
        }

        public function getType(): string
        {
            return Secondary::SECONDARY;
        }
    }
}
