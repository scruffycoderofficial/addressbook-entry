<?php

namespace CocoaStudios\AddressBook\Contact\Context
{
    use Ramsey\Uuid\Uuid;

    use CocoaStudios\AddressBook\Contact\Contract\Identity;
    use CocoaStudios\AddressBook\Entry\Contact\Type\Identifiable;

    /**
     * Class ContactId
     *
     * @package CocoaStudios\AddressBook\Contact\Context
     */
    final class ContactId implements Identity, Identifiable
    {
        private $contactId = null;

        public function contactId(): ContactId
        {
            if (is_null($this->contactId)) {
                return $this->fromFactory();
            } else {
                return $this->withIdentity($this->contactId);
            }
        }

        public function fromFactory(): ContactId
        {
            $contact = new self();

            $contact->setIdentity($this->createFromFactory());

            return $contact;
        }

        public function withIdentity(string $contactId): ContactId
        {
            if ($existingContactId = $this->setIdentity($contactId)->contactId()) {
                $contactId = $existingContactId;
            }

            $this->setIdentity($contactId);

            return $this;
        }

        public function setIdentity(string $identity): ContactId
        {
            $this->contactId = $identity;

            return $this;
        }

        private function createFromFactory()
        {
            return Uuid::uuid();
        }
    }
}
