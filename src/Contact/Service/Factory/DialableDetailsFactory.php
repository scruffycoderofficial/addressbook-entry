<?php

namespace CocoaStudios\AddressBook\Entry\Service\Factory
{
    use CocoaStudios\AddressBook\Entry\Factory\AbstractDialable;
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Dialable\DialablePrimary;
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Dialable\DialableSecondary;

    /**
     * Class DialableDetailsFactory
     *
     * @package CocoaStudios\AddressBook\Entry\Service\Factory
     */
    class DialableDetailsFactory extends AbstractDialable
    {
        protected $value;

        public function __construct($value)
        {
            $this->value = $value;
        }

        public function createPrimary(): DialablePrimary
        {
            return new DialablePrimary($this->value);
        }

        public function createSecondary(): DialableSecondary
        {
            return new DialableSecondary($this->value);
        }
    }
}
