<?php

namespace CocoaStudios\AddressBook\Entry\Service\Factory
{
    use CocoaStudios\AddressBook\Entry\Factory\AbstractMailable;
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Mailable\MailablePrimary;
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Mailable\MailableSecondary;

    /**
     * Class MailableDetailsFactory
     *
     * @package CocoaStudios\AddressBook\Entry\Service\Factory
     */
    class MailableDetailsFactory extends AbstractMailable
    {
        protected $value;

        public function __construct($value)
        {
            $this->value = $value;
        }

        public function createPrimary(): MailablePrimary
        {
            return new MailablePrimary($this->value);
        }

        public function createSecondary(): MailableSecondary
        {
            return new MailableSecondary($this->value);
        }
    }
}
