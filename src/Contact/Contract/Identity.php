<?php

namespace CocoaStudios\AddressBook\Entry\Contact\Contract
{
    use CocoaStudios\AddressBook\Contact\Context\ContactId;

    /**
     * Interface Identity
     *
     * @package CocoaStudios\AddressBook\Entry\Contact\Contract
     */
    interface Identity
    {
        /**
         * @return ContactId
         */
        public function contactId(): ContactId;

        /**
         * @return ContactId
         */
        public function fromFactory(): ContactId;
    }
}
