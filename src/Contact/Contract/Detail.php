<?php

namespace CocoaStudios\AddressBook\Entry\Contact\Contract
{
    /**
     * Interface ContactDetail
     *
     * @package CocoaStudios\AddressBook\Entry\Contact\Contract
     */
    interface Detail
    {
        /**
         * Name of this contact detail type e.g. Primary or Secondary types
         *
         * @return string
         */
        public function getName(): string;

        /**
         * Value of this contact detail
         *
         * @return string
         */
        public function getValue(): string;

        /**
         * Type of this contact detail as defined by either Dialable and/or Mailable types
         *
         * @see ContactTypes
         * @return string
         */
        public function getType(): string;
    }
}
