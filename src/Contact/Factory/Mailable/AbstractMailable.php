<?php

namespace CocoaStudios\AddressBook\Entry\Factory\Mailable
{
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Mailable\MailablePrimary;
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Mailable\MailableSecondary;

    /**
     * Class AbstractDialable
     *
     * @package CocoaStudios\AddressBook\Entry\Factory\Mailable
     */
    abstract class AbstractMailable
    {
        /**
         * @return MailablePrimary
         */
        abstract public function createPrimary() : MailablePrimary;

        /**
         * @return MailableSecondary
         */
        abstract public function createSecondary(): MailableSecondary;
    }
}
