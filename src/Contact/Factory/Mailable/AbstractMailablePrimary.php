<?php

namespace CocoaStudios\AddressBook\Entry\Factory\Mailable
{
    use CocoaStudios\AddressBook\Entry\Contact\Type\Detail\Primary;

    /**
     * Class AbstractMailablePrimary
     *
     * @package CocoaStudios\AddressBook\Entry\Factory\Mailable
     */
    abstract class AbstractMailablePrimary
    {
        /**
         * AbstractMailablePrimary constructor.
         *
         * @param $value
         */
        abstract public function __construct($value);

        /**
         * @return Primary
         */
        abstract public function getMailable(): Primary;
    }
}
