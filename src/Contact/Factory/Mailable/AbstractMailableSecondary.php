<?php

namespace CocoaStudios\AddressBook\Entry\Factory\Mailable
{
    use CocoaStudios\AddressBook\Entry\Contact\Type\Detail\Secondary;

    /**
     * Class AbstractMailableSecondary
     *
     * @package CocoaStudios\AddressBook\Entry\Factory\Mailable
     */
    abstract class AbstractMailableSecondary
    {
        /**
         * AbstractMailableSecondary constructor.
         *
         * @param $value
         */
        abstract public function __construct($value);

        /**
         * @return Secondary
         */
        abstract public function getMailable(): Secondary;
    }
}
