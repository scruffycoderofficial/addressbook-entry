<?php

namespace CocoaStudios\AddressBook\Entry\Factory
{
    use CocoaStudios\AddressBook\Entry\Contact\Contact;

    /**
     * Class AbstractContact
     *
     * @package CocoaStudios\AddressBook\Entry\Factory
     */
    abstract class AbstractContact
    {
        /**
         * @param array $data
         * @return Contact
         */
        abstract public function createContact(array $data): Contact;
    }
}
