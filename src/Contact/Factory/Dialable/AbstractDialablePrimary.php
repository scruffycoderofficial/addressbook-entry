<?php

namespace CocoaStudios\AddressBook\Entry\Factory\Dialable
{

    use CocoaStudios\AddressBook\Entry\Contact\Type\Detail\Primary;

    /**
     * Class AbstractDialablePrimary
     *
     * @package CocoaStudios\AddressBook\Entry\Factory\Dialable
     */
    abstract class AbstractDialablePrimary
    {
        /**
         * AbstractDialablePrimary constructor.
         *
         * @param $value
         */
        abstract public function __construct($value);

        /**
         * @return Primary
         */
        abstract public function getDialable(): Primary;
    }
}
