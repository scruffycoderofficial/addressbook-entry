<?php

namespace CocoaStudios\AddressBook\Entry\Factory
{
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Dialable\DialablePrimary;
    use CocoaStudios\AddressBook\Entry\Context\Concrete\Dialable\DialableSecondary;

    /**
     * Class AbstractDialable
     *
     * @package CocoaStudios\AddressBook\Entry\Factory
     */
    abstract class AbstractDialable
    {
        /**
         * @return DialablePrimary
         */
        abstract public function createPrimary() : DialablePrimary;

        /**
         * @return DialableSecondary
         */
        abstract public function createSecondary(): DialableSecondary;
    }
}
