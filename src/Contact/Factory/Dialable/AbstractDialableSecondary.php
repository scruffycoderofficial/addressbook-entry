<?php

namespace CocoaStudios\AddressBook\Entry\Factory\Dialable
{
    use CocoaStudios\AddressBook\Entry\Contact\Type\Detail\Primary;

    /**
     * Class AbstractDialableSecondary
     *
     * @package CocoaStudios\AddressBook\Entry\Factory\Dialable
     */
    abstract class AbstractDialableSecondary
    {
        /**
         * AbstractDialableSecondary constructor.
         *
         * @param $value
         */
        abstract public function __construct($value);

        /**
         * @return Primary
         */
        abstract public function getDialable(): Primary;
    }
}
