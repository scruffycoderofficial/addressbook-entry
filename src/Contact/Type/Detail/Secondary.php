<?php

namespace CocoaStudios\AddressBook\Entry\Contact\Type\Detail
{
    /**
     * Interface Secondary
     *
     * @package CocoaStudios\AddressBook\Entry\Contact\Type\Detail
     */
    interface Secondary
    {
        /**
         * Marks an entire entry of a Contact detail as secondary
         *
         * - SECONDARY entries are optional
         */
        const SECONDARY = 'secondary';

        /**
         * @return string
         */
        public function getName(): string;
    }
}
