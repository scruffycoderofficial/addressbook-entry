<?php

namespace CocoaStudios\AddressBook\Entry\Contact\Type\Detail
{
    /**
     * Interface Primary
     *
     * @package CocoaStudios\AddressBook\Entry\Contact\Type\Detail
     */
    interface Primary
    {
        /**
         * Marks an entire entry of a Contact detail as secondary
         *
         * - PRIMARY entries are mandatory
         */
        const PRIMARY = 'primary';

        /**
         * @return string
         */
        public function getName(): string;
    }
}
