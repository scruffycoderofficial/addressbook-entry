<?php

namespace CocoaStudios\AddressBook\Entry\Contact\Type
{

    use CocoaStudios\AddressBook\Contact\Context\ContactId;

    /**
     * Interface Identifiable
     *
     * @package CocoaStudios\AddressBook\Entry\Contact\Type
     */
    interface Identifiable
    {
        /**
         * @param string $identity
         * @return ContactId
         */
        public function setIdentity(string $identity): ContactId;
    }
}
