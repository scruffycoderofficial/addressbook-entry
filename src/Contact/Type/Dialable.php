<?php

namespace CocoaStudios\AddressBook\Entry\Contact\Type
{
    /**
     * Interface Dialable
     *
     * @package CocoaStudios\AddressBook\Entry\Contact\Type
     */
    interface Dialable
    {
        /**
         * A Mobile and/or Telephone Number type
         */
        const NUMBER = 'number';

        /**
         * @return string
         */
        public function getType(): string;
    }
}
