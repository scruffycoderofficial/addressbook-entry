<?php

namespace CocoaStudios\AddressBook\Entry\Contact\Type
{
    /**
     * Interface Mailable
     *
     * @package CocoaStudios\AddressBook\Entry\Contact\Type
     */
    interface Mailable
    {
        /**
         * An e-Mail Address type
         */
        const ADDRESS = 'email';

        /**
         * @return string
         */
        public function getType(): string;
    }
}
