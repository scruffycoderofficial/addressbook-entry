<?php

namespace CocoaStudios\AddressBook\Entry\Contact
{
    use CocoaStudios\AddressBook\Entry\Contact\Contract\Detail;

    /**
     * Interface Contact
     *
     * @package CocoaStudios\AddressBook\Entry\Contact
     */
    interface Contact
    {
        /**
         * Title of this Contact
         *
         * @return string
         */
        public function getTitle(): string;

        /**
         * First name of this Contact
         *
         * @return string
         */
        public function getFirstName(): string;

        /**
         * Last name of this Contact
         *
         * @return string
         */
        public function getLastName(): string;

        /**
         * All Contact's associated Mailables and Dialables details
         *
         * @return Detail[]
         */
        public function getDetails(): array;
    }
}
